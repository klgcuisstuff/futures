'From Cuis 5.0 [latest update: #4362] on 2 September 2020 at 1:50:08 pm'!
'Description '!
!provides: 'KlgFutures' 1 11!
SystemOrganization addCategory: #KlgFutures!


!classDefinition: #KlgFutureExecutor category: #KlgFutures!
Object subclass: #KlgFutureExecutor
	instanceVariableNames: 'process value priority monitor timeout hasException'
	classVariableNames: 'NoValue'
	poolDictionaries: ''
	category: 'KlgFutures'!
!classDefinition: 'KlgFutureExecutor class' category: #KlgFutures!
KlgFutureExecutor class
	instanceVariableNames: ''!

!classDefinition: #KlgFuture category: #KlgFutures!
ProtoObject subclass: #KlgFuture
	instanceVariableNames: 'executor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'KlgFutures'!
!classDefinition: 'KlgFuture class' category: #KlgFutures!
KlgFuture class
	instanceVariableNames: ''!


!KlgFutureExecutor commentStamp: '<historical>' prior: 0!
I am the executor for one future.!

!KlgFuture commentStamp: '<historical>' prior: 0!
I am a future.!

!KlgFuture methodsFor: 'printing' stamp: 'KLG 9/1/2020 19:20:26'!
printOn: aStream
	"Print me on `aStream`."

	aStream nextPutAll: self class name.
	executor isRunning
		ifTrue: [ aStream nextPutAll: '[running]' ]
		ifFalse: [
			executor hasException
				ifTrue: [
					aStream nextPut: ${.
					executor exception printOn: aStream.
					aStream nextPut: $} ]
				ifFalse: [
					aStream nextPut: $(.
					executor value printOn: aStream.
					aStream nextPut: $) ]].! !

!KlgFutureExecutor methodsFor: 'initialization' stamp: 'KLG 9/1/2020 14:55:52'!
defaultTimeout
	"Answer a sensible default time out in seconds"

	^ self class defaultTimeout! !

!KlgFutureExecutor methodsFor: 'initialization' stamp: 'KLG 9/1/2020 15:22:45'!
initialize
	"Initialize the monitor."

	super initialize.
	monitor _ Monitor new.
	value _ self class noValue.
	hasException _ false! !

!KlgFutureExecutor methodsFor: 'waiting' stamp: 'KLG 9/2/2020 12:38:36'!
wait
	"Wait for the monitor to be signaled."
	
	monitor critical: [
		hasException ifFalse: [
			value == self class noValue ifTrue: [
				monitor waitMaxSeconds: self timeout ]]].! !

!KlgFutureExecutor methodsFor: 'running' stamp: 'KLG 9/2/2020 13:00:44'!
run: aBlock
	"Run aBlock."

	| computedValue |
	self assert: process isNil.
	process _ [
		computedValue _ aBlock on: Exception do: [ :exception |
			monitor critical: [ 
				hasException ifFalse: [
					value _ exception.
					hasException _ true ] ].
			exception pass ].	
		monitor critical:  [
			hasException ifFalse: [			value _ computedValue ].
			monitor signalAll ] ] newProcess.
	process resumeAt: self priority! !

!KlgFutureExecutor methodsFor: 'running' stamp: 'KLG 9/2/2020 13:26:14'!
run: aBlock whenDone: aResultBlock
	"Run `aBlock` as a future, when there is value evalute `aResyltBlock`."

	self run: aBlock; wait.
	monitor critical: [
		^ aResultBlock valueWithPossibleArgument: value and: hasException ]! !

!KlgFutureExecutor methodsFor: 'running' stamp: 'KLG 9/1/2020 15:18:51'!
run: aBlock with: arguments
	"Run aBlock with arguments."

	self run: [ aBlock valueWithArguments: arguments ]! !

!KlgFutureExecutor methodsFor: 'accessing' stamp: 'KLG 9/1/2020 19:22:43'!
exception
	"Answer athe exception or nil"

	self isRunning ifTrue: [ ^ nil ].
	^ monitor critical: [ hasException ifTrue: [ value ] ifFalse: [ nil ] ]! !

!KlgFutureExecutor methodsFor: 'accessing' stamp: 'KLG 9/1/2020 14:25:47'!
priority
	"Answer the value of priority"

	^ priority ifNil: [ priority _ Processor userBackgroundPriority ]! !

!KlgFutureExecutor methodsFor: 'accessing' stamp: 'KLG 9/1/2020 14:06:14'!
priority: anObject
	"Set the value of priority"

	priority _ anObject! !

!KlgFutureExecutor methodsFor: 'accessing' stamp: 'KLG 9/1/2020 14:53:29'!
timeout
	"Answer the timeout"

	^ timeout ifNil: [ timeout _ self defaultTimeout ]! !

!KlgFutureExecutor methodsFor: 'accessing' stamp: 'KLG 9/1/2020 14:54:18'!
timeout: aTimeout
	"Set the timeout"

	self assert: timeout isNil.
	timeout _ aTimeout! !

!KlgFutureExecutor methodsFor: 'evaluating' stamp: 'KLG 9/2/2020 12:44:11'!
value
	"Answer the value of the execution."

	self wait.
	value == self class noValue ifTrue: [ self error: 'Execution timed out' ].
	hasException ifTrue: [ 
		"Me does not like this at all at all:"
		"So don't do it :}"
		"O: value instVarNamed: #signalContext put: nil."
		value error: 
			'Future signaled: "',
			(value messageText
				ifNil: [ value printString ]
				ifNotNil: [ :messageText | messageText ]),
			'"' ].
	^ value! !

!KlgFutureExecutor methodsFor: 'testing' stamp: 'KLG 9/1/2020 19:16:09'!
hasException
	"Answer true if we have an exception."

	^ monitor critical: [ hasException ]! !

!KlgFutureExecutor methodsFor: 'testing' stamp: 'KLG 9/1/2020 19:14:54'!
isRunning
	"Answer true if the executor is still running."

	^ monitor critical: [ value == self class noValue ]! !

!KlgFutureExecutor class methodsFor: 'running' stamp: 'KLG 9/1/2020 14:17:57'!
run: aBlock
	"Answer a future for a block. Execute it using an instance of mine."

	| answer executor |
	answer _ self futureClass new.
	executor _ self new.
	answer privateSetExecutor: executor.
	executor run: aBlock.
	^ answer! !

!KlgFutureExecutor class methodsFor: 'running' stamp: 'KLG 9/1/2020 14:58:13'!
run: aBlock timeout: aTimeout
	"Answer a future for a block. Execute it using an instance of mine."

	| answer executor |
	answer _ self futureClass new.
	executor _ self new.
	executor timeout: aTimeout.
	answer privateSetExecutor: executor.
	executor run: aBlock.
	^ answer! !

!KlgFutureExecutor class methodsFor: 'running' stamp: 'KLG 9/2/2020 13:34:14'!
run: aBlock whenDone: aResultBlock
	"Run `aBlock` as a future, when there is value evalute `aResyltBlock`."

	^ self new 	run: aBlock whenDone: aResultBlock! !

!KlgFutureExecutor class methodsFor: 'running' stamp: 'KLG 9/1/2020 14:17:50'!
run: aBlock with: arguments
	"Answer a future for a block. Execute it using an instance of mine."

	| answer executor |
	answer _ self futureClass new.
	executor _ self new.
	answer privateSetExecutor: executor.
	executor run: aBlock with: arguments.
	^ answer! !

!KlgFutureExecutor class methodsFor: 'running' stamp: 'KLG 9/1/2020 14:57:46'!
run: aBlock with: arguments timeout: aTimeout
	"Answer a future for a block. Execute it using an instance of mine."

	| answer executor |
	answer _ self futureClass new.
	executor _ self new.
	executor timeout: aTimeout.
	answer privateSetExecutor: executor.
	executor run: aBlock with: arguments.
	^ answer! !

!KlgFutureExecutor class methodsFor: 'instance creation' stamp: 'KLG 9/1/2020 14:55:57'!
defaultTimeout
	"Answer a sensible default time out in seconds"

	^ 30! !

!KlgFutureExecutor class methodsFor: 'instance creation' stamp: 'KLG 9/1/2020 14:14:04'!
futureClass
	"Answer the future class."

	^ KlgFuture! !

!KlgFutureExecutor class methodsFor: 'testing' stamp: 'KLG 9/1/2020 15:05:27'!
noValue
	"Answer a no value value that is different from nil."

	^ NoValue ifNil: [ NoValue _ Object new ]! !

!KlgFuture methodsFor: 'system primitives' stamp: 'KLG 9/1/2020 20:31:40'!
doesNotUnderstand: aMessage

	| value |
	value _ executor value.
	[ { self } elementsForwardIdentityTo: { value } ] fork.
	^ aMessage sendTo: value! !

!KlgFuture methodsFor: 'private' stamp: 'KLG 9/1/2020 14:11:16'!
privateSetExecutor: anExecutor
	"Set the executor"

	executor _ anExecutor! !

!KlgFuture methodsFor: 'printing' stamp: 'KLG 9/1/2020 19:56:39'!
displayStringOrText
	"To be used in the UI. Answer might be an instance of Text if appropriate.
	Don't include extra quotes for Strings.
	See the comments at:
		#printString
		#displayStringOrText
		#asString
		#storeString"

	^self printString! !

!KlgFuture methodsFor: 'printing' stamp: 'KLG 9/1/2020 19:55:55'!
fullPrintString
	"Answer a String whose characters are a description of the receiver."

	^String streamContents: [ :s | self printOn: s]! !

!KlgFuture methodsFor: 'printing' stamp: 'KLG 9/1/2020 19:52:33'!
printAs: streamType limitedTo: limit
	"Answer an instance of streamType whose characters describe the receiver.
	If you want to print without a character limit, use fullPrintString."

	| limitedString |
	limitedString _ streamType streamContents: [:s | self printOn: s] limitedTo: limit.
	limitedString size < limit ifTrue: [^ limitedString].
	^ limitedString , '[..]'! !

!KlgFuture methodsFor: 'printing' stamp: 'KLG 9/1/2020 19:51:48'!
printString
	"Answer a String whose characters are a description of the receiver. 
	If you want to print without a character limit, use fullPrintString.

	This description is to be meaningful for a Smalltalk programmer and usually includes
	a hint on the class of the object.

	Usually you should not reimplement this method in subclasses, but #printOn:

	See the comments at:
		#printString
		#displayStringOrText
		#asString
		#storeString"

	^ self printStringLimitedTo: 50000! !

!KlgFuture methodsFor: 'printing' stamp: 'KLG 9/1/2020 19:52:00'!
printStringLimitedTo: limit
	"Answer a String whose characters are a description of the receiver.
	If you want to print without a character limit, use fullPrintString."

	^self printAs: String limitedTo: limit! !

!KlgFuture methodsFor: 'printing' stamp: 'KLG 9/1/2020 19:51:17'!
printText
	^ Text streamContents: [:aStream| self printOn: aStream]! !

!KlgFuture methodsFor: 'class membership' stamp: 'KLG 9/1/2020 19:29:15'!
class
	"Primitive. Answer the object which is the receiver's class. Essential. See 
	Object documentation whatIsAPrimitive."

	<primitive: 111>
	self primitiveFailed! !

!BlockClosure methodsFor: '*KlgFutures' stamp: 'KLG 9/1/2020 14:20:52'!
future
	"Answer a future with me."

	^ KlgFutureExecutor run: self! !

!BlockClosure methodsFor: '*KlgFutures' stamp: 'KLG 9/1/2020 15:30:29'!
future: arguments
	"Answer a future with me and arguments."

	^ KlgFutureExecutor
		run: self
		with:
			(arguments isArray
				ifTrue: [ arguments ]
				ifFalse: [ {arguments} ]).! !

!BlockClosure methodsFor: '*KlgFutures' stamp: 'KLG 9/1/2020 15:30:11'!
future: arguments timeout: aTimeout
	"Answer a future with me and arguments."

	^ KlgFutureExecutor
		run: self
		with:
			(arguments isArray
				ifTrue: [ arguments ]
				ifFalse: [ {arguments} ])
		timeout: aTimeout.! !

!BlockClosure methodsFor: '*KlgFutures' stamp: 'KLG 9/1/2020 14:59:21'!
futureTimeout: aTimeout
	"Answer a future with me."

	^ KlgFutureExecutor run: self timeout: aTimeout ! !

!BlockClosure methodsFor: '*KlgFutures' stamp: 'KLG 9/2/2020 13:31:49'!
futureWhenDone: aResultBlock
	"Evalute `aResultBlock` with my value.."

	^ KlgFutureExecutor run: self whenDone: aResultBlock ! !
