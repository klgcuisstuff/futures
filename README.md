# Futures

Futures provides [Futures](https://en.wikipedia.org/wiki/Futures_and_promises) for Cuis.

## Usage

Future should be used with blocks:

```smalltalk
factorial := [ 10000 factorial ] future.
factorial.

[ :n | n factorial ] future: 1001.

[ 1000 factorial ] futureWhenDone: [ :result | Transcript cr; show: result ].
```

